/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example Drum Machine
 * This sketch shows how to use EventDelay to schedule tasks and how to use samples, simple 8 step drum machine
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>                          //include Mozzi Library core
#include <MozziSynthHardware.h>                 //include mozzi-synth Library

#include <Sample.h>                             //Sample template

#include <samples/bamboo/bamboo_03_2048_int8.h> // sample used as kick
#include <samples/bamboo/bamboo_01_2048_int8.h> // sample used as snare

Sample <BAMBOO_03_2048_NUM_CELLS, AUDIO_RATE> kick(BAMBOO_03_2048_DATA);
Sample <BAMBOO_01_2048_NUM_CELLS, AUDIO_RATE> snare(BAMBOO_01_2048_DATA);

#include <EventDelay.h>
EventDelay triggerDelay;

void setup() {
  pinMode(ardLED,OUTPUT);
  kick.setFreq((float) (BAMBOO_03_2048_SAMPLERATE / (float) BAMBOO_03_2048_NUM_CELLS)*0.25f); //play at original speed/4
  snare.setFreq((float) BAMBOO_01_2048_SAMPLERATE / (float) BAMBOO_01_2048_NUM_CELLS);        // play at the speed it was recorded at
  
  triggerDelay.set(200); // countdown ms, within resolution of CONTROL_RATE

  startMozzi(CONTROL_RATE);
}

byte counter = 0;
int pot1_prev;
void updateControl(){
  int pot1 = mozziAnalogRead(POT1);
  if(pot1 != pot1_prev){                        //if the potentiometer changed its value
      int trigDelay = map(pot1,0,1023,500,10);
      triggerDelay.set(trigDelay);              //set the delay between each trigger
    }
  pot1_prev = pot1;
    
  if(triggerDelay.ready()){
    switch(counter){
      case 0:
        digitalWrite(ardLED, HIGH);
        kick.start();
        break;
      case 1:
        break;
      case 2:
        snare.start();
        break;
      case 3:
        break;
      case 4:
        digitalWrite(ardLED, LOW);
        kick.start();
        break;
      case 5:
        kick.start();
        break;
      case 6:
        snare.start();
        break;
      case 7:
        break;
    }

    if(counter < 7){
      counter++;
    }
    else{
      counter = 0;
    }
    triggerDelay.start();
  }
}


AudioOutput_t updateAudio(){
  return MonoOutput::from8Bit(kick.next()+snare.next());
}

void loop(){
  audioHook();
}
