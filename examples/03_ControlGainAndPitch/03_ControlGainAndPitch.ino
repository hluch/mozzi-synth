/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example Control Gain and Pitch
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>            //include Mozzi Library core
#include <MozziSynthHardware.h>   //include mozzi-synth Library

#include <Oscil.h>                // include oscillator template


#include <tables/triangle2048_int8.h>
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> aOsc(TRIANGLE2048_DATA); //initialize triange oscillator

void setup() {
  aOsc.setFreq(220.f);    //set the initial frequency of the oscillator
  startMozzi();
}

byte gain = 0;

void updateControl(){
  int pot1 = mozziAnalogRead(POT1);         //read value of POT1 (0-1023)
  int pot2 = mozziAnalogRead(POT2);         //read value of POT2
  gain = map(pot1, 0, 1023, 0, 255);        //gain control is 8 bits (byte), map to 0-254 from 0-1023
  int freq = map(pot2, 0, 1023, 220, 440);  //map frequency control to 220 - 440 Hz from 0-1023

  aOsc.setFreq(freq);                       //set the frequency based on current value of POT2
}


AudioOutput_t updateAudio(){
  return MonoOutput::from16Bit(aOsc.next()*gain); //output from 16 bits - 8 bit audio * 8 bit gain
}

void loop(){
  audioHook();
}
