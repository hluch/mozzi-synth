/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example Tables
 * This sketch shows how to define a table of an oscillator.
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>            //include Mozzi Library core
#include <MozziSynthHardware.h>   //include mozzi-synth Library

#include <Oscil.h>                // include oscillator template

//SIN
#include <tables/sin2048_int8.h> // sine table for oscillator
Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aOsc(SIN2048_DATA);


/*//UNCOMMENT FOR TRIANGLE
#include <tables/triangle2048_int8.h>
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> aOsc(TRIANGLE2048_DATA);
*/

/*//UNCOMMENT FOR SAW
#include <tables/saw2048_int8.h>
Oscil <SAW2048_NUM_CELLS, AUDIO_RATE>  aOsc(SAW2048_DATA);
*/

/*//UNCOMMENT FOR SQUARE
#include <tables/square_no_alias_2048_int8.h>
Oscil <SQUARE_NO_ALIAS_2048_NUM_CELLS, AUDIO_RATE> aOsc(SQUARE_NO_ALIAS_2048_DATA);
*/

/*//UNCOMMENT FOR YOUR OWN TABLE
#include <tables/yourTableName.h>
Oscil <YOUR_TABLE_NUM_CELLS, AUDIO_RATE> aOsc(YOUR_TABLE_DATA);
*/

void setup() {
  startMozzi(CONTROL_RATE);                 //start with default control rate of 64
  aOsc.setFreq(440.f);                      //set initial frequency of the oscillator
}

void updateControl(){                       //nothing to control right now
}

AudioOutput_t updateAudio(){
  return MonoOutput::from8Bit(aOsc.next()); //return the oscillator output
}

void loop(){
  audioHook();                              //fill the audio buffer
}
