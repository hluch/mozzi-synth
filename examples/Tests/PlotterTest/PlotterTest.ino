/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - Plotter Test
 * In this example all 4 potentiometers are read and its' values are sent through serial port.
 * To test potentiometers, go to Tools -> Serial Plotter and start turning the knobs
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH Main Page      https://hluch.fel.cvut.cz
 * WS001 Repository     https://gitlab.fel.cvut.cz/hluch/ws001
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>            //include Mozzi Library core
#include <MozziSynthHardware.h>   //include mozzi-synth Library

void setup() {
    
  pinMode(ardLED,OUTPUT);

  Serial.begin(115200);
  startMozzi();
}


void updateControl(){
  
  int pot1 = mozziAnalogRead(POT1);
  int pot2 = mozziAnalogRead(POT2);
  int pot3 = mozziAnalogRead(POT3);
  int pot4 = mozziAnalogRead(POT4);
  
  
  Serial.print("POT1:");  Serial.print(pot1); //send these values through serial port to PC (Serial Monitor/Serial Plotter)
  Serial.print(",POT2:"); Serial.print(pot2);
  Serial.print(",POT3:"); Serial.print(pot3);
  Serial.print(",POT4:"); Serial.print(pot4);
  Serial.println();

  if(pot1 >= 512){              //if value of POT1 exceeds 511, turn ardLED on
    digitalWrite(ardLED, HIGH);
  }
  else{
    digitalWrite(ardLED,LOW);
  }

}


AudioOutput_t updateAudio(){
  //no audio to update in this example
}


void loop(){
  audioHook(); //even though there is no audio, we still need updateControl to run
}
