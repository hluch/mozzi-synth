/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example Random
 * This sketch shows random stuff. :D
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>                          //include Mozzi Library core
#include <MozziSynthHardware.h>                 //include mozzi-synth Library

#include <Oscil.h>

#include <tables/triangle2048_int8.h>
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> aOsc(TRIANGLE2048_DATA);
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> bOsc(TRIANGLE2048_DATA);

const float f0 = 261.63; //start at C4
const float a  = 1.059463;

float noteToFreq(int note,int octave,int root){
  return root*pow(a,note)*pow(2,octave);
}

//heptatonic scale
byte heptatonic[7] = {0,2,4,5,7,9,11};
byte tritonic[3] = {0,2,4};

#include <EventDelay.h>
EventDelay triggerDelay;

void setup() { 
  triggerDelay.set(200); // countdown ms, within resolution of CONTROL_RATE
  aOsc.setFreq(440.f);
  startMozzi(CONTROL_RATE);
}

int pot1_prev;

uint8_t counter = 0;

void updateControl(){
  int pot1 = mozziAnalogRead(POT1);
  
  if(pot1 != pot1_prev){                        //if the potentiometer changed its value
      int trigDelay = map(pot1,0,1023,500,10);
      triggerDelay.set(trigDelay);              //set the delay between each trigger
    }
  pot1_prev = pot1;
  
  if(triggerDelay.ready()){
  float freq = noteToFreq(heptatonic[rand()%7],rand()%3,f0);
  aOsc.setFreq(freq);
  counter++;
  if(counter == 7){
    bOsc.setFreq(noteToFreq(tritonic[rand()%3],-2,f0));
    counter = 0;
  }
  triggerDelay.start();
  }
}


AudioOutput_t updateAudio(){
  return MonoOutput::fromNBit(9,aOsc.next()+bOsc.next());
}

void loop(){
  audioHook();
}
