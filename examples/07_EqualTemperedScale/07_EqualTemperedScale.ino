/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example EqualTemperedScale
 * This sketch shows how to create the equal tempered scale with basic math
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>            //include Mozzi Library core
#include <MozziSynthHardware.h>   //include mozzi-synth Library

#include <Oscil.h>

#include <tables/triangle2048_int8.h>
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> aOsc(TRIANGLE2048_DATA);


const float f0 = 440.00;
const float a  = 1.059463;

//equal-tempered scale, A4 = 440 Hz (https://pages.mtu.edu/~suits/notefreqs.html)
//https://pages.mtu.edu/~suits/NoteFreqCalcs.html

void setup() {
  aOsc.setFreq(440.f);
  startMozzi(CONTROL_RATE);         //start with default control rate of 64
}

void updateControl(){
  int pot1 = mozziAnalogRead(POT1);
  pot1 = map(pot1,0,1020,0,12);     //1020 instead of 1023, this gets rid of some noise
  int pot2 = mozziAnalogRead(POT2);
  pot2 = map(pot2,0,1020,-3,2);
  float note = 440*pow(a,pot1)*pow(2,pot2);
  aOsc.setFreq(note);
}

AudioOutput_t updateAudio(){
  return MonoOutput::from8Bit(aOsc.next()); //return the oscillator output
}

void loop(){
  audioHook();                              //fills the audio buffer
}
