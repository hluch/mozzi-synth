/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Syntg WS001 - example Mix
 * This sketch shows how to mix multiple signals
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */

#include <MozziGuts.h>            //include Mozzi Library core
#include <MozziSynthHardware.h>   //include mozzi-synth Library

#include <Oscil.h>                // include oscillator template


#include <tables/triangle2048_int8.h>
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> aOsc(TRIANGLE2048_DATA); //initialize triange oscillator
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> bOsc(TRIANGLE2048_DATA); //initialize another oscillator

void setup() {
  aOsc.setFreq(220.00f);     //set the initial frequency of the oscillator
  bOsc.setFreq(293.66f);     // A3 and D4
  
  startMozzi(CONTROL_RATE); //start with default control rate of 64
}
byte gainA = 0;
byte gainB = 0;

void updateControl(){
  int pot1 = mozziAnalogRead(POT1);                                       //read value of POT1 (0-1023)
  int pot2 = mozziAnalogRead(POT2);                                       //read value of POT2
  
  gainA = map(pot1,0,1023,0,255);
  gainB = map(pot2,0,1023,0,255);

  /* //SIMPLER OPTION
  gainA = map(pot1, 0, 1023, 0, 127);                                     //gain control is 8 bits (byte), map to 0-127 from 0-1023
  gainB = map(pot2, 0, 1023, 0, 127);                                     //by having gain at most 127, there is no way to overflow the MonoOut, which is from16Bit - max number is 65535 
  */
}

AudioOutput_t updateAudio(){

  return MonoOutput::fromNBit(17,(long)(aOsc.next()*gainA)+(bOsc.next()*gainB));          //now the max signal will be (255*255)+(255*255) ... more than 2^16 but less than 2^17

  /* //SIMPLER OPTION
   return MonoOutput::from16Bit((aOsc.next()*gainA)+(bOsc.next()*gainB));                       //just add the two signals, max number will be (255*127)+(255*127) ... less than 2^16 ;
   */

}

void loop(){
  audioHook();              //fills the audio buffer
}
