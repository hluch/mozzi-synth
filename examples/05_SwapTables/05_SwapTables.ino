/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example Swap Tables
 * This sketch shows how to swap a table of an oscillator
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>                  //include Mozzi Library core
#include <MozziSynthHardware.h>         //include mozzi-synth Library

#include <Oscil.h>                      // include oscillator template

#include <tables/triangle2048_int8.h>   //both of these tables must have the same NUM_CELLS
#include <tables/saw2048_int8.h>

Oscil <2048, AUDIO_RATE> aOsc;          //here we dont define the table data, just the NUM_CELLS

bool usingTriangle = true;              //variable to keep track on which table is used

void setup() {
  aOsc.setFreq(220.00f);
  aOsc.setTable(TRIANGLE2048_DATA);                 //set the initial table
  startMozzi(CONTROL_RATE);                         //start with default control rate of 64
}

void updateControl(){
  int pot1 = mozziAnalogRead(POT1);                 //read potentiometer output
  if(pot1 < 512 && usingTriangle == false){         //if the pot value is less than 512 and currently saw is being used
    aOsc.setTable(TRIANGLE2048_DATA);               //set the the oscillator table to triangle
    usingTriangle = true;
  }
  else if(pot1 >= 512 && usingTriangle == true){    //if pot is greater than or equal to 512 and currently triangle is being used
    aOsc.setTable(SAW2048_DATA);
    usingTriangle = false;
  }
}

AudioOutput_t updateAudio(){
  return MonoOutput::from8Bit(aOsc.next());
}

void loop(){
  audioHook();              //fills the audio buffer
}
