/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH Mozzi Synth WS001 - example Heptatonic Scale
 * This sketch shows how to create a heptatonic scale from C
 * 
 * HLUCH Webpage        http://hluch.fel.cvut.cz/
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */
 
#include <MozziGuts.h>            //include Mozzi Library core
#include <MozziSynthHardware.h>   //include mozzi-synth Library

#include <Oscil.h>

#include <tables/triangle2048_int8.h>
Oscil <TRIANGLE2048_NUM_CELLS, AUDIO_RATE> aOsc(TRIANGLE2048_DATA);

const float f0 = 261.63; //start at C4
const float a  = 1.059463;

float noteToFreq(int note,int octave,int root){
  return root*pow(a,note)*pow(2,octave);
}

//heptatonic scale
byte heptatonic[7] = {0,2,4,5,7,9,11};

void setup() {
  aOsc.setFreq(440.f);
  startMozzi(CONTROL_RATE); //start with default control rate of 64
}

void updateControl(){

  int pot1 = map(mozziAnalogRead(POT1),0,1020,0,6);
  int pot2 = map(mozziAnalogRead(POT2),0,1020,-3,1);
  int pot3 = map(mozziAnalogRead(POT3),0,1020,0,6);
  float freq = noteToFreq(heptatonic[(pot1+pot3)%7],pot2+(pot1+pot3 >= 7),f0);
  aOsc.setFreq(freq);
}

AudioOutput_t updateAudio(){
  return MonoOutput::from8Bit(aOsc.next()); //return the oscillator output
}

void loop(){
  audioHook();                              //fills the audio buffer
}
