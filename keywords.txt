#######################################
# Syntax Coloring Map For the mozzi-synth Library
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################
MozziSynthHardware	KEYWORD1
HLUCH				KEYWORD1
#######################################
# Methods and Functions (KEYWORD2)
#######################################

#######################################
# Instances (KEYWORD2)
#######################################

#######################################
# Constants (LITERAL1)
#######################################
POT1	LITERAL1
POT2	LITERAL1
POT3	LITERAL1
POT4 	LITERAL1
ardLED	LITERAL1