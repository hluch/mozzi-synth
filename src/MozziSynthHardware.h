/*
 *__/\\\________/\\\__/\\\______________/\\\________/\\\________/\\\\\\\\\__/\\\________/\\\_        
 *__\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_____/\\\////////__\/\\\_______\/\\\_       
 *___\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\___/\\\/___________\/\\\_______\/\\\_      
 *____\/\\\\\\\\\\\\\\\_\/\\\_____________\/\\\_______\/\\\__/\\\_____________\/\\\\\\\\\\\\\\\_     
 *_____\/\\\/////////\\\_\/\\\_____________\/\\\_______\/\\\_\/\\\_____________\/\\\/////////\\\_    
 *______\/\\\_______\/\\\_\/\\\_____________\/\\\_______\/\\\_\//\\\____________\/\\\_______\/\\\_   
 *_______\/\\\_______\/\\\_\/\\\_____________\//\\\______/\\\___\///\\\__________\/\\\_______\/\\\_  
 *________\/\\\_______\/\\\_\/\\\\\\\\\\\\\\\__\///\\\\\\\\\/______\////\\\\\\\\\_\/\\\_______\/\\\_ 
 *_________\///________\///__\///////////////_____\/////////___________\/////////__\///________\///__
 *
 * HLUCH mozzi-synth - MozziSynthHardware.h
 * This file defines 4 connected potentiometers and the onboard LED
 * This file will be extended with more defines for future workshops.
 * 
 * HLUCH GitLab         https://gitlab.fel.cvut.cz/hluch
 * mozzi-synth          https://gitlab.fel.cvut.cz/hluch/mozzi-synth        - the mozzi-synth library
 * mozzi-synth-extras   https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras - build instructions, 3D models etc
 * Mozzi Main Page      https://sensorium.github.io/Mozzi/
 * Mozzi Documentation  https://sensorium.github.io/Mozzi/doc/html/index.html
 * 
 */

#ifndef __MSH__

	#define __MSH__

	const uint8_t POT1		= 0; //A0
	const uint8_t POT2		= 1; //A1
	const uint8_t POT3		= 2; //A2
	const uint8_t POT4		= 3; //A3
	
	const uint8_t ardLED	= 13; //D13

#endif // __MSH__