# HLUCH - Mozzi Synth library

#### Here you can find the Mozzi Synth library
Build instructions, 3D models etc can be found in the [mozzi-synth-extras](https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras) repository.

The Mozzi Synth library contains hardware mapping for the potentiometers and the LED.
More importantly it includes example codes, on which you can learn, how to code with [the Mozzi Library](https://sensorium.github.io/Mozzi/).

## Installation guide
1. Clone this repository or download it as .ZIP
2. Open Arduino IDE
3. Import the mozzi synth library by:
clicking on _Sketch -> Include library -> Add .ZIP Library -> Select the cloned mozzi-synth folder or .ZIP file -> Open_
Now you should be able to open the example codes by going to _Files -> Examples -> mozzi-synth_
4. Clone the Mozzi Library repository or download it as .ZIP from this [link](https://github.com/sensorium/Mozzi).
5. Import the Mozzi library by:
clicking on _Sketch -> Include library -> Add .ZIP Library -> Select the Mozzi-master folder or .ZIP file -> Open_
6. If everything went well, you should be able to see other example codes by going to _Files -> Examples -> Mozzi_
These examples will be useful to you later on as well, but they are not optimised for the hardware of Mozzi Synth. You might want to go here after you're done going through the mozzi-synth examples.
7. Now go the mozzi-synth Examples, open 01_BareMinimum.
8. Go to _Tools -> Board: Arduino Nano_
9. Click on Verify (blue check mark). If you see a message _Done compiling_, everything is installed correctly. Congrats!

Now you can connect the Mozzi Synth to the computer, select the right COM Port (_Tools -> Port_) and upload code.

## Useful links
[Hluch Website](http://hluch.fel.cvut.cz/)

[Hluch GitLab Repository](https://gitlab.fel.cvut.cz/hluch)

[mozzi-synth-extras Repository](https://gitlab.fel.cvut.cz/hluch/mozzi-synth-extras)

[Mozzi Main Page](https://sensorium.github.io/Mozzi/)

[Mozzi Download](https://github.com/sensorium/Mozzi)

[Mozzi Documentation](https://sensorium.github.io/Mozzi/doc/html/index.html)
